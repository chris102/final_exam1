// Shorthand for $( document ).ready()
$(function() {

    // ============== API DATA STRUCTURE ==================
    var api_data = {
      base_url: "https://newsapi.org/v2/",
      endpoint: ["everything", "top-headlines", "sources"],
      search_type: ["q="],
      language: "language=en",
      sort_by: ["popularity", "relevancy", "publishedAt"],
      apikey: "apiKey=67ea71f327964a74a479d48586c1d922"
    };
    // ====================================================

    // ====================================================
    // build a convenience function to render each article
    function render_article(article) {
      var str = "";
      // ... YOUR CODE TO BUILD ARTICLE HTML ...
      return str;
    }
    // ====================================================

    // ====================================================
    // Construct your search_url
    // ... YOUR CODE GOES HERE ...

  var news = $('#search_news').val();
    // ====================================================
  var search_url = "https://newsapi.org/v2/everything?q=search_pattern&language=en&sortBy=publishedAt&apiKey=apiKey=67ea71f327964a74a479d48586c1d922"
    // ====================================================
    // Perform the AJAX operation
    $.ajax({
      url: search_url,
      dataType: "json",
      success: function(data) {
        // Get the total number of articles
        var n = data["totalResults"];

        // Get the array of articles
        var articles = data["articles"];

        console.log("Total Articles Found:" + n);

        articles.forEach(function(article) {
          // Process all of the articles on this page
          render_article(article);
        });
      },
      error: function(e) {
        console.log("AJAX Error: " + e);
      }
    });
    // ====================================================
});
